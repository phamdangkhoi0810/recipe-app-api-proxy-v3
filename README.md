# recipe-app-api-proxy-v3
NGINX proxy for Recipe App

## Usage

### Environment variables

* `LISTEN_PORT`: port NGINX listen to (default to: `8000`)
* `APP_HOST`: hostname of the app to forward requests to (default: `app`)
* `APP_PORT`: port of the app (default: `9000`)


